﻿using Delegates.Extensions;
using System;
using System.Collections.Generic;

namespace Delegates
{
    public class WorkExtension
    {
        public void Run()
        {
            WriteMaxString();
            WriteMaxPay();
        }

        public void WriteMaxString()
        {
            List<string> listWords = new List<string>() { "Раз", "Два", "Три", "Четыре", "Пять" };

            Func<string, float> GetParameter = str => str.Length;
            Console.WriteLine($"Максимальное из слов в списке: {listWords.GetMax(GetParameter)}");
        }

        class UserPay
        {
            public string UserName { get; set; }
            public float Pay { get; set; }

            public override string ToString()
            {
                return $"UserName: {UserName}, Pay: {Pay}";
            }
        }

        public void WriteMaxPay()
        {
            List<UserPay> listUserPays = new List<UserPay>();
            listUserPays.Add(new UserPay() { Pay = 100, UserName = "User1" });
            listUserPays.Add(new UserPay() { Pay = 1000, UserName = "User2" });
            listUserPays.Add(new UserPay() { Pay = 10, UserName = "User3" });

            Func<UserPay, float> GetParameter = UserPay => UserPay.Pay;
            Console.WriteLine($"Максимальная плата в списке: {listUserPays.GetMax(GetParameter)}");
        }
    }
}
