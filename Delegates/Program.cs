﻿using System;
using System.Threading;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkExtension workExtension = new WorkExtension();
            workExtension.Run();

            Console.WriteLine();

            WorkFileSeeker workFileSeeker = new WorkFileSeeker();
            workFileSeeker.Run(AppDomain.CurrentDomain.BaseDirectory, 3);

            Console.ReadKey();
        }
    }
}