﻿using System;
using System.Collections;

namespace Delegates.Extensions
{
    public static class IEnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T maxT = default(T);

            float maxVal = float.MinValue;

            foreach (T elem in e)
            {
                float currentVal = getParameter(elem);
                if (maxVal <= currentVal)
                {
                    maxVal = currentVal;
                    maxT = elem;
                }
            }

            return maxT;
        }
    }
}
