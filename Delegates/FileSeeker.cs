﻿using System;
using System.IO;

namespace Delegates
{
    public class FileSeeker
    {
        public event EventHandler FileFound;
        public class FileArgs : EventArgs
        {
            public string FileName { get; set; }
        }

        public void Start(string targetDirectory)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(targetDirectory);
            foreach (FileInfo fileInfo in directoryInfo.EnumerateFiles())
            {
                FileArgs fileArgs = new FileArgs();
                fileArgs.FileName = fileInfo.Name;
                
                FileFound?.Invoke(this, fileArgs);
            }
        }
    }
}