﻿using System;

namespace Delegates
{
    public class WorkFileSeeker
    {
        private int _cntFilesForCancel;
        int counter = 0;
        FileSeeker fileSeeker;

        public void Run(string targetDirectory, int cntFilesForCancel)
        {
            _cntFilesForCancel = cntFilesForCancel;

            Console.WriteLine("File Seeker начал работу");

            fileSeeker = new FileSeeker();
            FileSeeker_Subscribe();
            fileSeeker.Start(targetDirectory);
            FileSeeker_Unsubscribe();

            Console.WriteLine("File Seeker закончил работу");
        }

        private void FileSeeker_FileFound(object sender, EventArgs e)
        {
            Console.WriteLine($"Найден файл: {((FileSeeker.FileArgs)e).FileName}");
            
            if (++counter == _cntFilesForCancel)
            {
                FileSeeker_Unsubscribe();
                Console.WriteLine("Поиск файлов отменен");
            }
        }

        private void FileSeeker_Subscribe()
        {
            fileSeeker.FileFound += FileSeeker_FileFound;
        }

        private void FileSeeker_Unsubscribe()
        {
            fileSeeker.FileFound -= FileSeeker_FileFound;
        }
    }
}
